//EXAMPLE 1:
/*
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main()
{
uint8_t num1 = 4, num2 = 5, res, resor, resxor, rescomp;
int8_t nums1 = -4, nums2 = -5, ress,ressor,ressxor,resscomp;

res = num1 & num2;
resor = num1 | num2;
resxor = num1 ^ num2;
rescomp = ~num1;


printf("RESAND : %d\n",res);
printf("RESOR : %d\n", resor);
printf("RESXOR : %d\n",resxor);
printf("RESCOMP : %d\n\n\n",rescomp);


ress = nums1 & nums2;
ressor = nums1 | nums2;
ressxor = nums1 ^ nums2;
resscomp = ~nums1;

printf("RESAND : %d\n",ress);
printf("RESOR : %d\n", ressor);
printf("RESXOR : %d\n",ressxor);
printf("RESCOMP : %d\n",resscomp);

return 0;
}
*/

#include <stdio.h>
#include<stdint.h>

int main()
{
	uint8_t num1 = 4, lshift1, rshift1;
	int8_t nums1 = -4,  lshift2, rshift2;

	lshift1 = num1 << 2;
	printf("%d << 2 = %d\n", num1,lshift1);

	lshift2 = nums1 << 2;
	printf("%d << 2 = %d\n", nums1, lshift2);

	rshift1 = num1 >> 2;
	printf("%d >> 2 = %d\n", num1, rshift1);

	rshift2 = nums1 >> 2;
	printf("%d >> 2 = %d\n", nums1, rshift2);

	return 0;
}


//EXERCISE 4:
/*
#include <stdio.h>
#include <stdint.h>

uint16_t rotateleft(uint16_t num, int n)
{
return ((num << n) | (num >> 16 -n));
}

uint16_t rotateright(uint16_t num, int n)
{
return ((num >> n) | (num << 16 -n));
}
int main ()
{
uint16_t num1 = 0x2345;

printf("Original: %0X\n", num1);
printf("Rotated Left: %0X\n", rotateleft(num1,4));
printf("Rotated right: %0X\n", rotateright(num1,4));
return 0;
}
*/





//EXERCISE 6:
/*
#include <stdio.h>
#include <stdint.h>
int main()
{
uint8_t a = 12, b = 15, c = 5, res;

res = a & b ^ c | a;
printf("%d\n",res);  // 13
res = (a & b) ^ (c | a);
printf("%d\n",res); //1

res = a << c | (a-b);
printf("%d\n",res);  //253

return 0;
}
*/
