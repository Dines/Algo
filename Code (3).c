// Write a C program that gets names and 3 scores of 5 students. Average score of the student will be written to the last element of score array.
/*#include <stdio.h>
int main()
{
	char names[5][20];
	double scores[5][4]; //3 scores, last element will keep the average score of the student
	int i, j;
	for (i = 0; i < 5; i++)
	{
		scores[i][3] = 0;
		scanf("%s", names[i]);
		for (j = 0; j < 3; j++)
		{
			scanf("%lf", &scores[i][j]);
			scores[i][3] += scores[i][j];
		}
		scores[i][3] /= 3;
		printf("%s %lf\n", names[i], scores[i][3]);
	}
	return 0;
}
*/
/*
#include <stdio.h>
#include <string.h>

int countingvowels(char word[])
{
	int i, count =0;
	for (i = 0; i < strlen(word); i++)
	{
		switch (word[i])
		{
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u': count++;
		}
	}
	return count;
}
void converttolowercase(char table[][100], int rows)
{
	int i, j;
		for (i = 0; i < rows; i++)
		{
			j = 0;
			while (table[i][j] != '\0' && (table[i][j] >= 65 && table[i][j] <= 90))
				table[i][j] += 32;
			j++;
	}
}
int main()
{
	char table[5][100];
	int i,j;
	for (i = 0; i < 5; i++)
	{
		scanf("%s", table[i]);
	}
	converttolowercase(table, 5);
	for (i = 0; i < 5; i++)
	{
		printf("%d\n",countingvowels(table[i]));
	}
	return 0;
}
*/
/*
#include <stdio.h>
#define ROW 5
#define COL 3

void fillMatrix(float table[])
{
	int i;
	for (i = 0; i < COL; i++) {
		scanf("%f", &table[i]);
	}
}
void printMatrix(float table[][COL])
{
	int i, j;
	for (i = 0; i < ROW; i++)
	{
		for (j = 0; j < COL; j++) {
			printf("%f ", table[i][j]);
		}
		printf("\n");
	}
}

int main()
{
	float matrix[ROW][COL];
	int i;
	for (i = 0; i < ROW; i++) {
		fillMatrix(matrix[i]);
	}
	printMatrix(matrix);
	return 0;
}
*/

#include <stdio.h>
#include <string.h>

void reversed(char words[][21])
{
	int i, j;
	for (i = 0; i < 6; i++)
	{
		for (j = strlen(words[i]) - 1; j >= 0; j--) {
			printf("%c", words[i][j]);
		}
		printf("\n");
	}
}
int main()
{
	char words[6][21];
	int i;
	for (i = 0; i < 6; i++){
		scanf("%s", words[i]);
	}
	reversed(words);
	return 0;
}