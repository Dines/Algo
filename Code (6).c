//EXERCISE 1
/*
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *name;
	int len;
	printf("How many characters does your name contain\n");
	scanf("%d", &len);
	name = (char *)calloc(len + 1, sizeof(char)); //allocate space in heap
	printf("Enter the name");
	scanf("%s", name);
	free(name);
	return 0;
}
*/

//EXERCISE 2
/*
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *name;
	int len;
	printf("How many characters does your name contain\n");
	scanf("%d", &len);
	name = (char *)calloc(len + 1, sizeof(char)); //allocate space in heap
	printf("Enter the name\n");
	scanf("%s", name);
	for (len = len - 1; len >= 0; len--) {
		printf("%c", *(name + len));
	}
	printf("\n");
	free(name);
	return 0;
}
*/

//EXERCISE 3
/*
#include <stdio.h>
#include <stdlib.h>

int main()
{
	char *name;
	int len;
	printf("How many characters does your name contain\n");
	scanf("%d", &len);
	name = (char *)calloc(len + 1, sizeof(char)); //allocate space in heap
	printf("Enter the name\n");
	scanf("%s", name);
	name = name + len - 1;

	while (len > 0)
	{
		
		printf("%c", *name);
		name--;
		len = len - 1;
	}
	name++;                  // ****name represents initial address of allocated space -1 so we have to increase it before using free ******
	printf("\n");
	free(name);
	return 0;
}
*/

//EXERCISE 4 ( It will rewrite the initial element
/*
#include <stdio.h>
#include <stdlib.h>

int main()
{
	float *p;
	float score;
	int count = 1;

	p = (float *)calloc(1, sizeof(float)); // Allocate a space

	printf("Enter the score\n");
	scanf("%f", &score);


	while (score != -1)
	{
		*p = score;
		// Allocate a new space for the new element 
		p = (float *)realloc(p, sizeof(float)*(count + 1));
		count = count + 1;
		scanf("%f", &score);
	}

	printf("Initial element : %f\n", *p);
	free(p);
	return 0;
}
*/


//EXERCISE 5 (REALLOC EXAMPLE PLEASE BE CAREFUL) (IT WILL GIVE THE ERROR, because of the realloc parameter of realloc must be the initial address of previously allocated area)
/*
#include <stdio.h>
#include <stdlib.h>

int main()
{
	float *p;
	float score;
	int count = 1;
	
	p = (float *)calloc(1, sizeof(float)); // Allocate a space

	printf("Enter the score\n");
	scanf("%f",&score);
	

	while (score != -1)
	{
		*p = score;
		// Allocate a new space for the new element 
		p = (float *) realloc(p, sizeof(float)*(count + 1));
		p = p + count;
		count = count + 1;
		scanf("%f", &score);
	}
              
	p = p - (count - 1);
	printf("Initial element : %f\n", *p);
	free(p);
	return 0;
}
*/


//EXERCISE 6 (REALLOC EXAMPLE PLEASE BE CAREFUL) (IT IS THE CORRECT VERSION, to realloc previously allocated area you have to give initial address)


#include <stdio.h>
#include <stdlib.h>

int main()
{
	float *p,*r;
	float score;
	int count = 1;

	p = (float *)calloc(1, sizeof(float)); // Allocate a space
	printf("Enter the score\n");
	scanf("%f", &score);

	r = p;    // Keep the initial address that you allocated 
	while (score != -1)
	{
		*p = score;
		// Allocate a new space for the new element 
		p = (float *)realloc(r, sizeof(float)*(count + 1));
		r = p;                                                //!!!!! Since we'll change the address of p, we have to keep it before we change it.!!!!!!!!!!!!!
		p = p + count;
		count = count + 1;
		scanf("%f", &score);
	}

	printf("Initial element : %f\n", *r);
	free(r);
	return 0;
}