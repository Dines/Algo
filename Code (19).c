1) Why do we need arrays? (You have to know this )

2) Write a C program that generates 20 strings randomly (maximum 10 characters) (Tip: generating capital char is the same with generating integers between 65 and 90), gets a string from user, 
calculates and prints the number of character matches for each string in the array (position of the characters must match). (rastgele 20 string oluşturuyorsunuz max 10 karakterli, kullanıcıdan bir string alıyorsunuz ve bu string ile oluşturduğunuz stringlerin her birini karşılaştırıp kaç karakterin eşleştiğini buluyorsunuz, karakterlerin string içindeki pozisyonları aynı olmak zorundadır)

3) After this course, We will have a friend John. For each week, we try to improve our algorithms to help him.

John is working in a library. His job is preparing a list of new books for each day. There are about 200 new books every day in the library. He needs an automation system to record author, title, and publishing year of the book (can be kept as string). 

4) Rewrite the program for 3rd question by using dynamic memory allocation function. The number of books, length of the author names and book names are unknown