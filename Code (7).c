/*Address of  the pointer*/

#include <stdio.h>
int main()
{
	int * *pt, *p, num = 4;
	char **ptch, *pch, ch = 'A';

	p = &num;
	pt = &p;
	pch = &ch;
	ptch = &pch;

	printf("Address of num %p, content of p %p, address of p %p, the value of pt %p\n", &num, p, &p, pt);

	printf("Address of ch %p, content of pch %p, address of pch %p, the value of ptch %p\n", &ch, pch, &pch, ptch);

	return 0;
}


/*Variable sized 30 element list (Get names of 30 students)*/
/*
#include <stdio.h>
#include <stdlib.h>
#define N 30
int main()
{
	char * names[N]; // 30 elements including char pointer
	int i, len; // loop variable and length of the name

	for (i = 0; i < N; i++)
	{
		//For each student ask the length of the name,  allocate space, and get the name
		printf("The length of your name:\n");
		scanf("%d", &len);
		names[i] = (char *)calloc(len + 1, sizeof(char)); // Allocate space
		printf("Enter your name:\n");
		scanf("%s", names[i]);  //names[i] keeps the address of the allocated space for ith name
	}
	printf("Entered names\n");
	for (i = 0; i < N; i++)
	{
		printf("%s\n", names[i]);
	}
	// Free the allocated spaces
	for (i = 0; i < N; i++)
	{
		free(names[i]);
	}
	return 0;
}
*/

/*Variable sized variable length list (Get names of students, the number of student and their names' length are unknown */
/*
#include <stdio.h>
#include <stdlib.h>
int main()
{
	char ** names; //Dynamically allocated length and for each element dynamically allocated size
	int n, len, i;  // # of students, length of the names, loop variable

	printf("How many students do you have:\n");
	scanf("%d", &n);
	names = (char **)calloc(n, sizeof(char *)); // we need n char pointer to keep the names of the students
	for (i = 0; i < n; i++)
	{
		printf("Enter the length of the %dth name\n", (i + 1));
		scanf("%d", &len);
		names[i] = (char *)calloc(len + 1, sizeof(char));
		printf("Enter the name\n");
		scanf("%s", names[i]);
	}
	printf("Entered names:\n");
	for (i = 0; i < n; i++)
	{
		printf("%s\n", *(names + i));
	}
	// First, release each array
	for (i = 0; i < n; i++)
	{
		free(*(names+i));
	}
	// Release the list
	free(names);
	return 0;

}*/
/*

#include <stdio.h>
#include <stdlib.h>
int main()
{
	char  **pnames; //Dynamically allocated length and for each element dynamically allocated size
	int n, len, i, count = 0;  // # of students, length of the names, loop variable

	
	pnames = (char **)calloc(1, sizeof(char *)); // we need n char pointer to keep the names of the students
	scanf("%d", &n);
	while ( n != -1)
	{
		printf("Enter the length of the name\n");
		scanf("%d", &len);
		pnames[count] = (char *)calloc(len + 1, sizeof(char));
		printf("Enter the name\n");
		scanf(" %s", pnames[count]);
		count = count + 1;
		pnames = (char **)realloc(pnames,(count + 1)*sizeof(char *));
		scanf("%d", &n);
	}
	printf("Entered names:\n");
	for (i = 0; i < count; i++)
	{
		printf("%s\n", *(pnames + i));
	}
	// First, release each array
	for (i = 0; i < count; i++)
	{
		free(*(pnames + i));
	}
	// Release the list
	free(pnames);
	return 0;

}
*/
