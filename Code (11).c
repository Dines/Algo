//EXAMPLE 1: enum
/*#include <stdio.h>

int main()
{
	enum color { RED, BLUE, GREEN, WHITE };
	enum week { MON = 1, TUE, WED, THURS, FRI, SAT, SUN };
	enum { space = ' ', comma = ',', colon = ':', point = '.', exclamation = '!', semicolon = ';' }; //Their ASCII values
	enum mycolor { PINK, ROSE = 0, CRIMSON = 0, SCARLET = 3, MAGENTA, AQUA = 1, NAVY = 1, BLACK, JADE = 2, CYAN };
	//!!!!!! PINK = 0, ROSE = 0, CRIMSON = 0, SCARLET = 3, MAGENTA = 4, AQUA = 1,NAVY = 1, BLACK=2, JADE=2,CYAN =3 !!!!!!!!!!!
	enum color xcolor, ycolor, zcolor;
	enum week	day;


	xcolor = RED;
	ycolor = WHITE;
	//	zcolor = PURPLE; //Error It is not defined

//	printf("x:%d	y:%d\n", xcolor, ycolor);

//	ycolor = BLUE;
//	zcolor = GREEN;
//	printf("x: %d	y:%d	z:%d\n", xcolor, ycolor, zcolor);

	switch (ycolor)
	{
	case RED:	printf("RED\n"); break;
	case WHITE: printf("WHITE\n"); break;
	case BLUE:	printf("BLUE\n"); break;
	case GREEN:	printf("GREEN\n"); break;
	}
	
	zcolor = (enum color)2;
	printf("x: %d	y:%d	z:%d\n", xcolor, ycolor, zcolor);

	printf("Color inputs: 0-3\n");
	scanf("%d%d%d", &xcolor, &ycolor, &zcolor);
	printf("x: %d	y:%d	z:%d\n", xcolor, ycolor, zcolor);

	printf("Days inputs: 1-7, Color inputs: 0-3\n");
	scanf("%d %d", &day, &xcolor);
	printf("Your%cday is %dth day %c and your color is %dth color %c\n", space, day, exclamation, xcolor, point);
	printf("Other enums: %c		%c		%c\n\n", comma, colon, semicolon);
	
	printf("PINK\t%d\nROSE\t%d\nCRIMSON\t%d\nSCARLET\t%d\nMAGENTA\t%d\nAQUA\t%d\nNAVY\t%d\nBLACK\t%d\nJADE\t%d\nCYAN\t%d\n", PINK, ROSE, CRIMSON, SCARLET, MAGENTA, AQUA, NAVY, BLACK, JADE, CYAN);
	return 0;
}
*/

//EXAMPLE 2: Union Little Endian or Big Endian Memory Placement
//Little Endian: The least significant byte -> in the smallest address
//Big Endian: The most significant byte -> in the smallest address

//If you see A before B, your architecture uses Big Endian
//If you see B before A, your architecture uses Little Endian
/*
#include <stdio.h>

typedef union
{
	short num;  //2bytes
	char  charArry[2]; // 1 byte for each element
}SH_CH2;

int main(void)
{
	SH_CH2 data;

	data.num = 16706;   //binary :01000001 -> 65 ASCII of A 01000010 -> 66 ASCII of B
	printf("%d\n", sizeof(data.num));
	printf("Short: %d\n", data.num);
	printf("Ch[0]: %c\n", data.charArry[0]); 
	printf("Ch[1]: %c\n", data.charArry[1]);
	return 0;
}
*/
//EXAMPLE 3: Unions in structure

#include <stdio.h>
#include <string.h>

typedef struct
{
	char first[20];
	char init;
	char last[20];
} PERSON;

typedef struct
{
	char type;
	union
	{
		char company[40];
		PERSON person;
	}un;
}NAME;

int main(void)
{
	NAME business = { 'C',"ABC Company" };
	NAME frnd;
	NAME names[2];
	int		i;

	printf("Size of struct PERSON = %d and size of union un = %d and size of NAME = %d\n", sizeof(frnd.un.person), sizeof(frnd.un), sizeof(frnd));
	frnd.type = 'P';
	strcpy(frnd.un.person.first, "Martha");
	strcpy(frnd.un.person.last, "Frage");
	frnd.un.person.init = 'H';
	names[0] = business;
	names[1] = frnd;

	for (i = 0; i < 2; i++)
	{
		switch (names[i].type)
		{
		case 'C': printf("Company :%s\n", names[i].un.company); break;
		case 'P': printf("Friend  :%s %c %s\n", names[i].un.person.first, names[i].un.person.init, names[i].un.person.last); break;
		default: printf("Error in type\a\n"); break;
		}
	}
	printf("\n");
	return 0;
}