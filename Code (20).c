void * malloc(size_t totalsize)


Allocates area with totalsize (in bytes) the area is not initialized.

(totalsize ile belirtilen bayt kadar yer ayırır ve bu alanın başlangıç değeri bilinemez.)




void * calloc (int nelement, size_t sizeofthelement)


Allocates area with nelement*sizeoftheelement, the area is initialized with 0 or \0.

(eleman sayısı * eleman boyutu kadar yer ayırır, alan 0 ile başlatılır)



void * realloc(void *ptr, size_t newsize)
(ptr must be initial address of previously allocated space (Check the error in Ex 5), it allocates an area with newsize. It can return different initial address.
If there is not available space for the new size, it allocates spaces in different address and it frees previously allocated space. So the returned address is the 
new address)

(ptr daha önce ayrılmış bir alanın başlangıç adresi olmalıdır (Bknz Ör 5'deki hata), newsize kadar yer ayırır. Her zaman aynı adresi dönmek zorunda değildir.
Yer olmazsa farklı bir adresten başlamak üzere yeniden yer ayırır ve eski yeri serbest bırakır. Artık başlangıç adresi değişmiştir.)




void free(void *ptr)

to release the allocated space. ptr must be the initial address of the allocated area.
(Tahsis edilen alanı serbest bırakır. ptr daha önce tahsis edilmiş alanın başlangıç değeri olmalıdır.)