
/*#include <stdio.h>

typedef struct
{
	
	char name[26];
	char id[11];
	int grade;
} STUDENT;


int main()
{
	STUDENT std;
	printf("Enter the student id\n");
	scanf("%s", std.id);
	printf("Enter the student name\n");
	scanf("%s", std.name);
	printf("Enter the grade\n");
	scanf("%d", &std.grade);

	printf("%s %s %d\n", std.id, std.name, std.grade);
	return 0;
}
*/

/*
#include <stdio.h>
#define N 3

typedef struct
{

	char name[26];
	char id[11];
	int grade;
} STUDENT;


int main()
{
	STUDENT std[N];
	int i;
	for (i = 0; i < N; i++) {
		printf("Enter the student id\n");
		scanf("%s", std[i].id);
		printf("Enter the student name\n");
		scanf("%s", std[i].name);
		printf("Enter the grade\n");
		scanf("%d", &std[i].grade);
	}
	for (i = 0; i < N; i++) {
		printf("%s %s %d\n", std[i].id, std[i].name, std[i].grade);
	}
	return 0;
}
*/


#include <stdio.h>
#define N 3

typedef struct
{

	char *book;
	char isbn[14];
	int *author;
} BOOK;


int main()
{
	BOOK b[N];
	int i, len;
	for (i = 0; i < N; i++) {
		printf("# of characters ->book name\n");
		scanf("%d",&len);
		b[i].book = (char *)calloc(len + 1, sizeof(char));
		printf("Enter the book name\n");
		scanf("%s", b[i].book);
		printf("# of characters ->author name\n");
		scanf("%d", &len);
		b[i].author = (char *)calloc(len + 1, sizeof(char));
		printf("Enter the author name\n");
		scanf("%s", b[i].author);
		printf("Enter the isbn\n");
		scanf("%s", b[i].isbn);
	}
	for (i = 0; i < N; i++) {
		printf("%s %s %s\n", b[i].book, b[i].author, b[i].isbn);
		free(b[i].book);
		free(b[i].author);
	}
	return 0;
}