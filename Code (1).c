/*
#include<stdio.h>
int main()
{
	char str[] = { 'A','b','c','d','e','f','g' }; //Char array
	char str2[] = "Abcdefg";                     //String
	printf("%d\n", sizeof(str)); //7
	printf("%d\n", sizeof(str2)); //8
	return 0;
}
*/

#include <stdio.h>
int main()
{
	char str[] = "cen", str2[8]; //str :String, str2 :char array
	int i;
	printf("%s\n", str);
	scanf("%s", str);  //get string
	printf("%s\n", str);
	scanf("%s", str2);  //str2 is used as string. get string 7 chars can be input
	printf("%s\n", str2);
	for (i = 0; i < 8; i++) {
		scanf("%c ", &str2[i]);   //str2 is used as character array
	}
	printf("%s", str2); // Since str2 was used as character array and there is not \0. Output includes meaningless characters.

	// If you used character array, you must use loops to get and print the values.
	// If you want to use %s you have to consider \0 (termination character)
	return 0;
}