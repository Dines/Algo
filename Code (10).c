//INDIRECTION OPERATOR
/*
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char name[26];
	char id[11];
	float gpa;
} STUDENT;

int main()
{
	STUDENT * stdnt;
	int choice = 0, count = 0, i;
	stdnt = (STUDENT *)calloc(1, sizeof(STUDENT));
	while (choice == 0)
	{
		printf("Enter id, name, gpa\n");
		scanf("%s ", stdnt->id);
		scanf("%s ", stdnt->name);
		scanf("%f", &stdnt->gpa);
		stdnt = stdnt - count;                                           // To be sure that it is the initial address
		count++;
		stdnt = (STUDENT *)realloc(stdnt, (count + 1) * sizeof(STUDENT));          // Realloc requires initial address
		stdnt = stdnt + count;                                                // Initial address + number of records to be able to write the new record in the new place
		printf("To continue to enter, use 0; to exit enter -1");
		scanf("%d", &choice);
	}
	stdnt = stdnt - count;                                                 // Go to initial address
	for (i = 0; i < count; i++){
		printf("%s %s %f\n", stdnt[i].id, stdnt[i].name, stdnt[i].gpa);
	}
	free(stdnt);
	return 0;
}
*/

//INDIRECTION OPERATOR 2
/*
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char *name;
	char id[11];
	float gpa;
} STUDENT;

int main()
{
	STUDENT * stdnt;
	int choice = 0, count = 0, len, i;
	stdnt = (STUDENT *)calloc(1, sizeof(STUDENT));
	while (choice == 0)
	{
		printf("Enter the length of the name\n");
		scanf("%d", &len);
		stdnt->name = (char *)calloc(len + 1, sizeof(char));
		printf("Enter id, name, gpa\n");
		scanf("%s", stdnt->id);
		scanf("%s", stdnt->name);
		scanf("%f", &stdnt->gpa);
		stdnt = stdnt - count;
		count++;
		stdnt = (STUDENT *)realloc(stdnt, (count + 1) * sizeof(STUDENT));
		stdnt = stdnt + count;
		printf("To continue to enter, use 0; to exit enter -1");
		scanf("%d", &choice);
	}
	stdnt = stdnt - count;
	for (i = 0; i < count; i++) {
		printf("%s %s %f\n", stdnt[i].id, stdnt[i].name, stdnt[i].gpa);
	}
	for (i = 0; i < count; i++)
		free(stdnt[i].name);
	free(stdnt);
	return 0;
}*/


//LINKED LIST

#include <stdio.h>
#include <stdlib.h>

typedef struct BOX
{
	char *msg;
	int id;
	struct BOX * next;

} MSGBOX;

MSGBOX *first = NULL, *last = NULL;               //Keep address of beginning of the list in first, keep address of end of the list in last

void insert(MSGBOX * note)
{
	MSGBOX * temp, * previous = NULL;
	temp = first;
	while (temp != NULL && temp->id <= note->id)      //Find the appropriate place for the new record
	{
			previous = temp;
			temp = temp->next;
	}
		//Insert note between previous and temp

	if (previous == NULL) {                     // The new record should be placed first place 
		if (first == NULL) {                    //list is empty
			first = note;
			last = note;
			last->next = NULL;
		}
		else                                 //list is not empty new record will be placed to first place
		{
			first = note;
			first->next = temp;
		}
	}
	else                                       //new record will be placed between two records or to the end of the list         
	{
		previous->next = note;             
		note->next = temp;
		if (previous == last)               // new record will be placed into the end of the list
			last = note;
	}
}

void list()
{
	MSGBOX *temp = first;
	while (temp != NULL)
	{
		printf("%d %s\n", temp->id, temp->msg);
		temp = temp->next;
	}
}

int main()
{
	MSGBOX * box, *temp = first, *previous = NULL;
	int choice = 0, i, count = 0, len;
	while (choice == 0)
	{
		box = (MSGBOX *)calloc(1, sizeof(MSGBOX));
		printf("Enter the length of the message\n");
		scanf("%d", &len);
		box->msg = (char *)calloc(len + 1, sizeof(char));
		printf("Enter id and message\n");
		scanf("%d", &box->id);
		scanf("%s", box->msg);
		insert(box);   // Insert the message into the list
		printf("To continue to enter, use 0; to exit enter -1");
		scanf("%d", &choice);
	}
	list();
	while (temp != NULL)
	{
		free(temp->msg);
		previous = temp->next;
		free(temp);
		temp = previous;
	}
	
	return 0;
}



//OPTIONAL EXERCISE

/*#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char name[26];
	char id[11];
	float gpa;
} STUDENT;

void save(STUDENT *std, int count)
{
	printf("Enter id, name, and gpa\n");
	scanf("%s", std[count].id);
	scanf("%s", std[count].name);
	scanf("%f", &std[count].gpa);
}

void list(STUDENT *std, int count)
{
	int i;
	for (i = 0; i < count; i++)
		printf("%s %s %f\n", std[i].id, std[i].name, std[i].gpa);
}

float average(STUDENT *std, int count)
{
	float avg = 0;
	int i;
	for (i = 0; i < count; i++)
		avg += std[i].gpa;
	
	return avg/count;
}
int main() 
{
	int count = 0;
	int choice = 0;
	STUDENT * stdnt; //We don't know how many student we will have

	stdnt = (STUDENT *)calloc(1, sizeof(STUDENT));
	while (1)
	{
		printf("1- Save, 2 -List, 3- Calculate Average, 4- Exit\n");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: save(stdnt, count); count++; stdnt = (STUDENT *)realloc(stdnt, sizeof(STUDENT)*(count + 1)); break;
		case 2: list(stdnt, count); break;
		case 3: printf("Average is %f\n", average(stdnt, count)); break;
		case 4: free(stdnt); exit(0);
		}
	}

	return 0;
}
*/