
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifndef REVERSE
#define REVERSE
#endif
char * reverse(char *str)
{
	char *sstr;
	int i = strlen(str) - 1;
	sstr = (char *)calloc(i + 1, sizeof(char));
	for (; i >= 0; i--) {
		*sstr = *(str + i);
		sstr++;
	}
	*sstr = '\0';
	sstr = sstr - (strlen(str));
	return sstr;
}
