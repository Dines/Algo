// EXERCISE 1: Preprocessor Command (File inclusion)
/*
#include "Reverse.h"

int main()
{
	char *string;

	string = (char *)calloc(50, sizeof(char));
	strcpy(string, "I Love Programming");
	printf("%s\n", reverse(string));
	free(string);
}
*/

// EXERCISE 2: Preprocessor Command (Define Constants)
/*
#include <stdio.h>
#define ROW 3
#define COL 2

int main()
{
	int ary[ROW][COL], i, j;

	for (i = 0; i < ROW; i++) {
		for (j = 0; j < COL; j++)
			ary[i][j] = i*j + i;
	}

	for (i = 0; i < ROW; i++) {
		for (j = 0; j < COL; j++) {
			printf("%d\t", ary[i][j]);
		}
		printf("\n");
	}

}
*/

//EXERCISE 3: Preprocessor Commands (Simulating Functions)
/*
#include <stdio.h>

#define PRODUCT(x,y)\
x*y
int main()
{
	int a = 5, b = 4;

	printf("%d\n", PRODUCT(a, b));
}

*/

//EXERCISE 4: Preprocessor Commands (Simulating Functions)
/*
#include <stdio.h>

#define POWER2(x)\
1 << (x)
int main()
{
	int a = 5;

	printf("%d\n", POWER2(a));
}
*/

//EXERCISE 5: Preprocessor Commands (Undefined Macros)
/*
#include <stdio.h>
#define SIZE 10

int main()
{
	printf("%d\n", SIZE);
	#undef SIZE
	printf("%d\n", SIZE); // SIZE is not defined
}
*/

//EXERCISE 6: Preprocessor Commands (Predefined Macros)
/*
#include <stdio.h>

int main()
{

	printf("DATE AND TIME : %s %s\n", __DATE__,__TIME__);
	printf("LINE %d\n", __LINE__);
	printf("file %s\n", __FILE__);
	return 0;
}
*/
//EXERCISE 7: Preprocessor Commands (OPerator Related to Macros)
/*
#include <stdio.h>

#define PRINT_VAL(a, type) printf(#a " contains :" #type,a);
#define PRINT(b) printf("Enter a value for " #b);  //String converting operator
#define FORM(T,N) T##N  //Merge Operator
#define PI 3.14
int main()
{
	int x = 5, y = 8, z, FORM(A,1) = 1;
	float FORM(A, 2); //Create a variable name
	char FORM(A, 3) = 'k';
#ifdef PI
	A2 = PI;
#else
	A2 = 9.9;
#endif
	PRINT(z)
	scanf("%d", &z);
	PRINT_VAL(x, %d\n)
	PRINT_VAL(y, %d\n)
	PRINT_VAL(z, %d\n)
	PRINT_VAL(A1,%d\n)
	PRINT_VAL(A2,%f\n)
	PRINT_VAL(A3,%c\n)
}
*/


// EXERCISE 11: Preprocessor Command (File inclusion)

#ifndef REVERSE
#include "Reverse.h" // Userdefined header file
#endif
int main()
{
	char *string;

	string = (char *)calloc(50, sizeof(char));
	strcpy(string, "I Love Programming");
	printf("%s\n", reverse(string));
	free(string);
}