
#include <stdio.h>
#include <string.h>
void converttoLOWER(char str[], int len)
{
	int i;
	for (i = 0; i < len; i++) {
		if (str[i] >= 65 && str[i] <= 90)
			str[i] += 32;
	}
}

int countingvowels(char str[], int len)
{
	int i, count = 0;
	for (i = 0; i < len; i++)
	{
		switch (str[i])
		{
		case 97: 
		case 101:
		case 105:
		case 111:
		case 117: count++;
		}
	}
	return count;
}

int main()
{
	char str[100];

	printf("Enter a word:\n");
	scanf("%s", str);
	converttoLOWER(str,strlen(str));
	printf("Number of vowels %d\n", countingvowels(str, strlen(str)));

	return 0;
}