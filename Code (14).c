//EXERCISE 1: TEXT files
/*
#include <stdio.h>
int main()
{
	FILE * fptr;
	int number = 111;
	char c = 'b';
	fptr = fopen("CEN1162017.txt", "w+"); // It is a text file and it is opening for reading and writing if it does not exist it will create
	if (fptr == NULL)
		exit(-1);
	fprintf(fptr, "%d %c",number,c); //It will print to file pointed by fptr 111 and b.
	rewind(fptr);	//It takes the cursor to beginning of the file
	fscanf(fptr, "%d %c", &number,&c); // It reads data from file and converts first part to integer and stores it in memory
	printf("%d %c\n", number,c);
	fclose(fptr); // Size of text file will be 5 bytes although number is integer it is stored as char
	return 0;
}
*/
//EXERCISE 2: Text files
/*
#include <stdio.h>
#include <stdlib.h>

int main()
{
	FILE * fp; //File struct pointer
	char name[50], r_name[10];
	int id, r_id;
	char info[100];
	char key;

	fp = fopen("cen116-2.txt", "w+");
	if (!fp)
		exit(-1);

	fputs("Hello World!", fp);
	fputs("Hello\n", fp);
	fputc('D', fp);
	fputc('\n', fp);
	printf("Enter id and name\n");
	scanf("%d", &id);
	scanf(" %s", name);

	fprintf(fp, "%d %s", id, name);

	//READ
	rewind(fp);   //return to the beginning
	fgets(info, sizeof(info), fp); //read until \n
	printf("%s", info);
	key = fgetc(fp);
	printf("%c\n", key);
	fscanf(fp, " %d %s", &r_id, r_name);
	printf("%d %s\n", r_id, r_name);

	fclose(fp); //Close file
	return 0;
}
*/
//EXERCISE 3: Fseek and ftell used for text file
/*
#include <stdio.h>
#include <string.h>
int main()
{
	FILE *fp;
	int i, rnum;
	long int place;
	char * str = "CHANGED", strr[8] = { '\0' };

	//Open file with read+write permission, if file does not exist it will be created, 
	//otherwise its contents will be overwritten 

	fp = fopen("cen116text.txt", "w+");
	//Each line will include a number and \n; 
   //hence the size of the file will be for \n (consists of \n and \r)-> 200 bytes +
   //0 - 9 10 bytes +10-99 180 bytes 
	for (i = 0; i < 100; i++)
	{

		if (fprintf(fp, "%d\n", i) == NULL)
			printf("Error\n");
	}
	// ftell gives the current position of the cursor,
	// we expect 390 because after writing our cursor will be at the end
	place = ftell(fp);
	printf("Indicator is at %ld bytes, it means end of file\n", place);
	fseek(fp, 0, SEEK_SET); // It takes the cursor to the beginning
	//Until you reach the End Of File  read line by line
	while (fscanf(fp, "%d", &rnum) != EOF)
	{
		printf("RNUM:%d\n", rnum);
	}
	//Move the cursor from beginning to twelve bytes after 
	// 0 -> 3 bytes so after 3 we will insert 1000 it takes 4 + 2 bytes 6 bytes
	// 1000 will be written to the places of 4 and 5
	fseek(fp, 12, SEEK_SET); 
	i = 1000;
	if (fprintf(fp, "%d\n", i) == NULL)
		printf("Error\n");
	rewind(fp);
	printf("\nAFTER CHANGES FSCANF:\n");
	//You will see 1000 instead of 4 and 5
	while (fscanf(fp, "%d", &rnum) != EOF)
	{
		printf("RNUM:%d\n", rnum);
	}
	//Take the cursor to the end 
	fseek(fp, 0, SEEK_END);
	//Add a new str to the end
	if (fprintf(fp, "%s", str) == NULL)
		printf("ERROR\n");
	//Move the cursor from end to the beginning of the lastly added string
	fseek(fp, -1 * (strlen(str) * sizeof(char)), SEEK_END);
	if (fscanf(fp, "%s", strr) != NULL);
	printf("LAST ELEMENT: %s\n", strr);
	fclose(fp);
	return 0;
}
*/
//Exercise 4: Binary Files
/*
#include <stdio.h>
int main()
{
	FILE * fptr;
	int num[10], rnum[10], i;
	char ch = 'b';

	fptr = fopen("Cen116BinaryFiles-2.txt", "wb+");

	for (i = 0; i < 10; i++)
	{
		printf("Enter a number:\n");
		scanf("%d", &num[i]);
	}
	if (fwrite(num, sizeof(int), 10, fptr) == 0)
		printf("It cannot be written\n");
	if (fwrite(&ch, sizeof(char), 1, fptr) == 0)
		printf("It cannot be written\n");

	rewind(fptr); //Move the cursor to the beginning

	if (fread(rnum, sizeof(int), 10, fptr) == 0)
		printf("It cannot be read\n");
	printf("\n \n");
	for (i = 0; i < 10; i++)
	{
		printf("%d\n",rnum[i]);
	}
	if (fread(&ch, sizeof(char), 1, fptr) == 0)
		printf("It cannot be read\n");
	printf("%c\n", ch);

	fclose(fptr);
	return 0;
}
*/
//Exercise 5: Binary Files
/*
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	char name[50];
	char surname[50];
	int id;
	int score[2];
}STUDENT; // sizeof structure 50+50+4+8= 112

int main()
{
	FILE * fp; //File struct pointer
	STUDENT stu, stuinfo;

	char sname[50];
	char ssurname[50];
	int sid;
	int sscore[2];
	int place;

	stu.id = 123;  
	printf("Enter name, surname and two scores\n");
	scanf("%49s %49s %d %d", stu.name, stu.surname, stu.score, stu.score + 1);

	fp = fopen("cen116binary.txt", "wb+");
	if (!fp)
		exit(-1);

	if (fwrite(&stu, sizeof(STUDENT), 1, fp) != 1) //1 is count
		printf("Error, it cannot be written\n");
	//The size of the file will be 112 bytes because it is a binary file. 
	//We can learn it by using ftell because our cursor is at the end of the file
	//and ftell returns the position of the cursor
	place = ftell(fp);
	printf("Cursor at %ld byte\n", place);
	//READ
	rewind(fp);   //return to the beginning

	//Read from file to variable stuinfo
	if (fread(&stuinfo, sizeof(STUDENT), 1, fp) == 0)
		printf("It cannot be read\n");
	

	printf("ID: %d NAME: %s SURNAME: %s SCORE: %d %d\n", stuinfo.id, stuinfo.name,stuinfo.surname,stuinfo.score[0], stuinfo.score[1]);


	fclose(fp); //Close file
	return 0;
}*/

//Exercise 6: Binary Files Ftell Fseek

#include <stdio.h>
#include <string.h>
int main()
{
	FILE *fp;
	int i, num, rnum;
	long int place;
	char * str = "CHANGED", strr[8] = { '\0' };

	fp = fopen("cen116binary3.txt", "wb+");

	for (i = 0; i < 100; i++)
	{
		num = i;
		if (fwrite(&num, sizeof(int), 1, fp) != 1)
			printf("Error\n");
	}
	// Size of file will be 100*4 = 400 bytes
	place = ftell(fp);
	printf("Indicator is at %ld bytes\n", place);
	// Move the cursor to the beginning
	fseek(fp, 0, SEEK_SET);
	while (fread(&rnum, sizeof(int), 1, fp)) // Until reach to the end read all values 
	{
		printf("RNUM:%d\n", rnum);
	}
	fseek(fp, 3 * sizeof(int), SEEK_SET); //3*4 bytes from beginning -> After 2, add 1000 instead of 3
	num = 1000;
	if (fwrite(&num, sizeof(int), 1, fp) != 1)
		printf("Error\n");
	fseek(fp, -(10 * sizeof(int)), SEEK_END);
	if (fread(&rnum, sizeof(int), 1, fp) != 0)
		printf("-10*sizeof(int)+END value:%d\n", rnum);
	fseek(fp, -(3 * sizeof(int)), SEEK_CUR);
	if (fread(&rnum, sizeof(int), 1, fp) != 0)
		printf("-3*sizeof(int)+CUR value:%d\n", rnum);
	rewind(fp);
	printf("\nAFTER CHANGES\n");
	while (fread(&rnum, sizeof(int), 1, fp))
	{
		printf("RNUM:%d\n", rnum);
	}

	fseek(fp, 0, SEEK_END);
	if (fwrite(str, (strlen(str) * sizeof(char)), 1, fp) != 1)
		printf("ERROR\n");

	fseek(fp, -1 * (strlen(str) * sizeof(char)), SEEK_END);
	fread(strr, (strlen(str) * sizeof(char)), 1, fp);
	printf("LAST ELEMENT: %s\n", strr);

	fclose(fp);
	return 0;
}

/*
#include<stdio.h>
#include<stdlib.h>
#include <string.h>

typedef struct
{
	char name[60];
	double gpa;
}STUDENT;

int main()
{
	STUDENT stu;
	FILE *fd;
	char nameread[60];
	char surnameread[40];
	double gparead;
	fd = fopen("a.txt", "a+");
	fgets(stu.name, 60, stdin);
	scanf("%lf", &stu.gpa);
	fprintf(fd, "%s %lf\n", stu.name, stu.gpa);
	rewind(fd);
	while (fscanf(fd, "%s %s %lf", nameread, surnameread, &gparead) != EOF) {
		printf("%s %s %lf\n", nameread, surnameread, gparead);
	}
	fclose(fd);
	return 0;
}
*/
/*
#include<stdio.h>
#include<stdlib.h>
#include <string.h>


int main()
{
	
	FILE *fd;
	char nameread[60];
	char surnameread[40];
	char searchname[60];
	double gparead;
	scanf("%s", searchname); //fgets also reads\n and strcmp will fail
	fd = fopen("a.txt", "r");
	while (fscanf(fd, "%s %s %lf", nameread, surnameread, &gparead) != EOF) {
		if (strcmp(searchname, nameread) == 0) {
			printf("GPA: %lf\n", gparead);
		}
	}
	fclose(fd);
	return 0;
}*/